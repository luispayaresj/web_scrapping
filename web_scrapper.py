from bs4 import BeautifulSoup
import requests
import re 


search_term = input("What product do you want to search for?")

url = f"https://www.alkosto.com/computadores-tablet/computadores-portatiles/c/BI_104_ALKOS"
page = requests.get(url).text 
doc = BeautifulSoup(page, "html.parser")

tags_nav_ul= doc.find(class_="pagination--buttons")
pages = int(tags_nav_ul.ul['data-numbers'])
print(f'Hay {pages} paginas')

computers = {}

count = 0

for page in range(1, pages + 1):
    print("----------------------------------------------------------")
    print(f"Page #{page}")
    
    url = f"https://www.alkosto.com/computadores-tablet/computadores-portatiles/c/BI_104_ALKOS?page={page}"
    single_page = requests.get(url).text 
    doc = BeautifulSoup(single_page, "html.parser")
    div = doc.find(class_="product__listing product__list")
    dirty_items = div.find_all(string=re.compile(search_term) )
    items = []

    for dirty in dirty_items:
        if dirty.startswith(search_term):
            items.append(dirty)

    
    for item in items:
        count += 1
        print(f"Item #{count}")
        print("____________________________")
        parent = item.parent
        link = None
        if parent.name == "a":
            link = "https://www.alkosto.com"+parent['href']
        
        parent_2 = item.find_parent(class_="product__list--item product__list--alkosto")
        
        price_string = str(parent_2.find(class_="price").string)
        
        price = int((price_string).replace("$","").replace(".",""))
        
        parent_3 = item.find_parent(class_="product__information")

        specifications_set = parent_3.find_all(class_="item--value")

        if len(specifications_set) < 3 :
            ram = 0
            store = 0
            processor = 'Does not apply'
        else :
            ram_string = str(specifications_set[0].string)
            ram = int((ram_string.replace(" GB","")))

            store_string = str(specifications_set[1].string)
            store = int((store_string.replace(" GB","")))

            processor= str(specifications_set[2].string)
            name = str(item.string).split("-")[-0]

            if search_term in ['AMD','Intel']:
                name = name.split(f'{search_term}')[-0]

        

        print(f'Name: {name}')
        print(f"Product's Link: {link}")
        print(f'Ram: {ram}')
        print(f'Store: {store}')
        print(f'Processor: {processor}')
        print(f'Price: ${price}')

        key = f"computer{count}"
        computers[key] = {'name':name, 'link':link,'ram':ram,'store':store,'processor':processor,'price':price}
print(computers)